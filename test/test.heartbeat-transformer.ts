import { config as loadConfigAsEnvVar } from 'dotenv';
const { error } = loadConfigAsEnvVar();

if (error) {
    throw error;
}

import { connect as connectAmqp } from 'amqplib';
import { v4 } from 'uuid';

const UUID = v4();

const amqpConnectionConf = {
    protocol: 'amqp',
    hostname: process.env.PLETHORA_IP,
    port: Number(process.env.RABBITMQ_PORT),
    username: process.env.RABBITMQ_USER,
    password: process.env.RABBITMQ_PASS,
    vhost: process.env.RABBITMQ_VHOST,
};
const q = process.env.AMQP_QUEUE || 'plethora';

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/PLETHORA_DB';
const MESSAGES_COLLECTION = 'messages';

const PLETHORA_TOPIC = 'plethora_topic';
const PLETHORA_TOPIC_PREFIX = 'plethora';

const PLETHORA_TOPIC_NAME = 'plethora.HEARTBEAT';

connectAmqp(amqpConnectionConf)
    .then(amqpConnection => amqpConnection.createChannel())
    .then((channel) => {
        channel.assertExchange(PLETHORA_TOPIC, 'topic', { durable: false });
        function publishMessage() {
            channel.publish(
                PLETHORA_TOPIC,
                PLETHORA_TOPIC_NAME,
                new Buffer(JSON.stringify({
                    device_id: UUID,
                    module_id: 'HEARTBEAT',
                    timestampe: new Date().getTime(),
                    data: {
                        key: 'HEARTBEAT',
                        value: 'UP',
                    },
                })),
            );
            console.log(
                '[RABBITMQ_LISTENER]#plethora_topic Sent message to %s',
                PLETHORA_TOPIC_NAME,
            );

            setTimeout(
                publishMessage,
                1000,
            );
        }

        publishMessage();

    })
    .catch (err => console.error(err));

import { config as loadConfigAsEnvVar } from 'dotenv';
const { error } = loadConfigAsEnvVar();

if (error) {
    throw error;
}

import { connect as connectAmqp, Message } from 'amqplib';
import { MongoClient, Db } from 'mongodb';

import { IMessage } from '@plethora/types';
import { AmqpConnection } from '@plethora/utils';

const amqpConnectionConf = {
    protocol: 'amqp',
    hostname: process.env.PLETHORA_IP,
    port: Number(process.env.RABBITMQ_PORT),
    username: process.env.RABBITMQ_USER,
    password: process.env.RABBITMQ_PASS,
    vhost: process.env.RABBITMQ_VHOST,
};
const exchange = process.env.AMQP_EXCHANGE || 'plethora_topic';

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017M/DB_PLETHORA';
const DEVICES_COLLECTION = 'devices';
const TOPIC_NAME = 'plethora.HEARTBEAT';
const HEARTBEAT_TIMEOUT = process.env.HEARTBEAT_TIMEOUT || 30000;
const G_TIMEOUT: { [k: string]: any } = {};

const amqpConnection = new AmqpConnection(amqpConnectionConf);

function setDeviceDown(db: Db, deviceId: string) {
    return () => {
        db.collection(DEVICES_COLLECTION)
            .updateOne(
                { device_id: deviceId },
                { $set: { status: false } },
                { upsert: true },
            )
            .then(() => {
                console.log('Updated status of device %s to false', deviceId);
            });
    };
}

MongoClient.connect(mongoUrl)
    .then((db: Db) => Promise.all([
        db,
        amqpConnection.retrieveTopicMessage(
            exchange,
            TOPIC_NAME,
        ),
    ]))
    .then(([db, msgObservable]) => {
        console.log('listen...');
        // There is a subscription that is not handled
        msgObservable
            .subscribe((msg: Message) => {
                const parsedMsg: IMessage = JSON.parse(msg.content.toString());
                console.log('Received message', parsedMsg);
                const deviceId = parsedMsg.device_id;
                if (G_TIMEOUT[deviceId])
                    clearTimeout(G_TIMEOUT[deviceId]);
                db.collection(DEVICES_COLLECTION)
                    .findOne({ device_id: deviceId })
                    .then((device) => {
                        if (!device) {
                            return db.collection(DEVICES_COLLECTION).insertOne({
                                device_id: deviceId,
                                status: true,
                            })
                            .then(() => Promise.resolve());
                        }

                        if (!device.hasOwnProperty('status') || !device.status) {
                            return db.collection(DEVICES_COLLECTION)
                                .updateOne(
                                    { device_id: deviceId },
                                    { $set: { status: true } },
                                )
                                .then(() => Promise.resolve());
                        }
                    })
                    .then(() => {
                        G_TIMEOUT[deviceId] = setTimeout(
                            setDeviceDown(db, deviceId),
                            HEARTBEAT_TIMEOUT,
                        );
                        console.log('Updated status of device %s', parsedMsg.device_id);
                    });
            });
    })
    .catch((err) => { console.error(err); });

function clearTimeouts() {
    Object.keys(G_TIMEOUT)
        .forEach((timeoutId: any) => {
            clearTimeout(G_TIMEOUT[timeoutId]);
        });
    process.exit();
}

['SIGINT', 'SIGQUIT', 'SIGTERM', 'uncaughtException']
    .forEach((signal: any) => {
        process.on(signal, clearTimeouts);
    });
